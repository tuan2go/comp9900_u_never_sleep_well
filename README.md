﻿##Setup environment and framework

###Setup environment
1. The version of python:
```
python 3.6.4
```
2. Create an virtual environment for Flask
```
python -m venv flask
```
3. Install Flask and other extension packages
```
    cd flask
    python -m pip install --upgrade pip
    pip install flask
    pip install flask-login
    pip install flask-mail
```
*If you choose use virtural environment, 
you need to use the commond 
<code>flask/Scripts/python run.py</code>
to run your program, so I suggest you install these packages on your local environment for better debug experience*


###Setup framework
1. Pull the latest repostory and run the applicaiton
```
   git pull origin master
   python run.py
```
2. File structure
```
Work Station
    *   flask --environment
    *   webapp
        *   static --:css, js, image files
        *   templates -- html files
        *   _init_.py -- file for initialize this application
        *   views.py -- view funcions
        *   classes.py -- write class files for each module
    *   run.py -- run and debug this application
```