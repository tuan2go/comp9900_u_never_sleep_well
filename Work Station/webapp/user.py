from datetime import datetime
import webapp.commonFunction as common
from flask import render_template, session, url_for,request,redirect, make_response,flash,jsonify,send_file,Blueprint

from werkzeug.security import generate_password_hash, check_password_hash

from webapp import app


from bson import json_util, ObjectId
import json
from hashlib import md5
from flask_pymongo import ObjectId

from tempfile import NamedTemporaryFile
from shutil import copyfileobj
from os import remove
from io import StringIO
from PIL import Image, ImageDraw, ImageFont
import random
import string
import os

loginPage = "login.html"
registerPage = "register.html"
registerPage2 = "register2.html"
registerPage3 = "register3.html"
indexPage="index.html"
userPage="user.html"
settingPage = "setting.html"
mypropertyPage = "mypro.html"
myorderPage = "myorder.html"

mongo = common.initMongo()





@app.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'POST':
        email = request.form.get("email")
        password = request.form.get("password")
        response = AccountCheck(email, password)

        if response['result'] == 'success':
            curr_user = FindAll(email)
            curr_user_sanitized = json.loads(json_util.dumps(curr_user))
            username = curr_user['username']
            print('username:', username)
            userid = str(curr_user['_id'])
            print('userid', userid)
            # flash(response['message'])
            session['curr_user'] = curr_user_sanitized
            session['email'] = email
            session['username']=username
            session['userid'] =userid
            return redirect(url_for("index", username=username))
        elif response['result'] == 'fail':
            flash(response['message'])
        else:
            flash(response['message'])
        print(email,password)
    return render_template(loginPage)

def FindAll(email):
    res = mongo.db.users.find({'email':email})
    result = list(res)[0]
    print("findall",result)
    return result

def AccountCheck(email, password):
    result = mongo.db.users.find({'email': email})
    try:
        list_result = list(result)[0]
        if result.count() > 0 and list_result['status'] == 'f':
            return dict(result='success', message='login successfully!') \
                if check_password_hash(list_result['password'],password) \
                else dict(result='fail', message='Your password is not correct!')
        else:
            return dict(result='error', message='Email does not exist or you are banned from administration!')
    except:
        return dict(result='error', message='Email does not exist or you are banned from administration!')


def CheckValidEmail(email):
    result = mongo.db.users.find({'email':email})
    if result.count() > 0:
        return dict(result = 'error',message='Email already exists, please login')
    else:
        return dict(result='success',message='Email is valid!')


@app.route('/register',methods=['GET','POST'])
def register():
    print("-----")
    if request.method == 'POST':

        email = request.values.get("email")
        username= request.values.get("username")
        password = request.values.get("password")
        password_hash = generate_password_hash(password)
        createtime = datetime.now()
        code=request.values.get("code")
        response= CheckValidEmail(email)

        if code != session["real_verification_code"]:
            code_vaild=0
            flash('code is not correct.')
        else:
            code_vaild=1
        if response['result'] == 'error':
            flash(response['message'])
        print(email)
        if response['result'] == 'success'and username is not None and code_vaild==1:
            usericon = avatar(email, 128)
            print('usericon',usericon)
            user={
              "email":email,
              "username":username,
              "password":password_hash,
              "createtime":createtime,
              "confirmed":False,
              "comfirmedtime":None,
                "status":'f',
              "usericon":usericon,
                "reviews_id":[],
                "phone":"",
                "description":"",

                "gender":"",
                "birth_date":"",


                "booked_id":[]

              }

            mongo.db.users.insert_one(user)
            #session[user]=user
            token = generate_confirmation_token(email)
            confirm_url = url_for('confirm_email', token=token, _external=True)
            html = render_template('activate.html', confirm_url=confirm_url)
            subject = "Please confirm your email"
            msg_typ="html"
            common.sendEmail(email,subject,msg_typ,html)
            #login_user(user)
            print("send email ok")
            #flash('A confirmation email has been sent via email.', 'success')
            print(user)
            return redirect(url_for('register2'))

    return render_template(registerPage)


@app.route('/register2',methods=['GET', 'POST'])
def register2():
    return render_template(registerPage2)

@app.route('/register3',methods=['GET', 'POST'])
def register3():
    return render_template(registerPage3)
#give new user a usericon
def avatar(email, size):
    digest = md5(email.lower().encode('utf-8')).hexdigest()
    return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

#@login_required  ,methods=['POST']
@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    session.pop('email',None)
    session.pop('userid',None)
    session.pop('curr_user',None)
    return render_template(indexPage)
###########################################user profile###################################
@app.route('/user',methods=['GET', 'POST'])
def userprofile():
    if not session.get('username'):
        return redirect(url_for('login'))
    else:
        username = session['username']
        usericon = session['curr_user']['usericon']
        email = session['email']
        userid = session['userid']
    if request.method == 'POST':
        userimage = request.files.get("usericon")
        if userimage:
            basepath = app.config['basedir'] + '/user_image/' + str(userid)
            if not os.path.exists(basepath):
                 os.mkdir(basepath)
            filename = userimage
            path = os.path.join(basepath, filename)
            userimage.save(path)
            mongo.db.users.update({'email': email}, {"$set": {"usericon": path}})
        print('image',userimage)
        phone = request.form.get("phone_number")
        gender = request.form.get("gender")
        birth = request.form.get("birth_date")
        description = request.form.get("user_describe")
        mongo.db.users.update({'email': email}, {"$set": {"phone":phone,"gender":gender,"birth":birth,"description":description}})
        flash('change accept!')
    return render_template(userPage, username=username, usericon=usericon)

@app.route('/setting',methods=['GET', 'POST'])
def setting():
    print('000000000000000')
    if not session.get('username'):
        return redirect(url_for('login'))
    else:
        username = session['username']
        usericon = session['curr_user']['usericon']
        email = session['email']
        print('11111111111111')
    if request.method == 'POST':
        print('22222222222')
        curr_password = request.form.get("curr_pw")
        new_password = request.form.get("new_pw")
        print('cur_password',curr_password)
        res = mongo.db.users.find({'email': email})
        password = list(res)[0]['password']
        if check_password_hash(password, curr_password):
            mongo.db.users.update({'email': email}, {"$set": {"password": generate_password_hash(new_password)}})
            flash('change password successful! ')
        else:
            flash('password is not correct!')
    return render_template(settingPage,username=username,usericon=usericon)




@app.route('/myorder',methods=['GET', 'POST'])
def myorder():
    if not session.get('username'):
        return redirect(url_for('login'))
    else:
        username = session['username']
        usericon = session['curr_user']['usericon']
        userid = session['userid']
        result = mongo.db.book.find({'user_id': userid})
    #     result = mongo.db.book.find({'user_id': userid})
    #     order= list(result)
    #     order_list=[]
    #     if order:
    #         for i in order:
    #             order_detial=mongo.db.post.find({'_id': ObjectId(i['property_id'])})
    #             print('order_detial:',order_detial)
    # return render_template(myorderPage,username=username,usericon=usericon,order=order_list)


@app.route('/mypro',methods=['GET', 'POST'])
def myproperty():
    if not session.get('username'):
        return redirect(url_for('login'))
    else:
        username = session['username']
        usericon = session['curr_user']['usericon']
        userid = session['userid']
        result = mongo.db.post.find({'userid': userid})
        mylist = list(result)
        book_time=[]
        if result:
            myproperty = json.loads(json_util.dumps(mylist))
            for i in myproperty:
                property_id = i['_id']['$oid']
                b_result=mongo.db.book.find({'property_id': property_id})
                if b_result:
                    b_list = list(b_result)
                    print('test_what',b_list)
                    for j in b_list:
                        print('test_b')
        else:
            myproperty=result

    return render_template(mypropertyPage, username=username, usericon=usericon, myproperty=myproperty,book_time=book_time)
#需要加路径？
def changeusername():
    if session.get('username'): #if user is login and can get username
        email = session['email'] # find user in db though session email
        new_username = request.form.get("username")
        mongo.db.users.update({'email': email}, {"$set": {"username": new_username}})
        flash('change username successful!')
    else:
        flash('Please login first!')






#############验证码################
@app.route('/verification_code')
def serve_img():
    img, content = get_code()
    print("content11111: ",content)
    print("__________")
    session["real_verification_code"] = content
    print(session["real_verification_code"],"session")

    basepath = app.config['basedir']+'/static/images/'
    path = os.path.join(basepath, 'verification_code.jpg')
    img.save(path)
    print("save path")


    tempFileObj = NamedTemporaryFile(mode='w+b', suffix='jpg')
    pilImage = open(path, 'rb')
    copyfileobj(pilImage, tempFileObj)
    pilImage.close()
    remove(path)
    tempFileObj.seek(0, 0)

    response = send_file(tempFileObj, as_attachment=True, attachment_filename='verification_code.jpg')

    return response
def serve_pil_image(pil_img):
    img_io = StringIO()
    pil_img.save(img_io, 'JPEG', quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')


def get_code(width=100, height=40, fontSize=25):
    """
    width: 背景图片的宽度
    height:背景图片的高度
    fontsize：验证码的字体大小
    """
    img = Image.new("RGB", (width, height), getColor())  # 创建指定大小，背景颜色，模式的图片
    draw = ImageDraw.Draw(img)  # 创建画刷
    path = get_Font()  # 得到一个系统下的随机字体路径
    print("path1111111:",path)
    # 获取指定路径的字体
    font = ImageFont.truetype(font=path, size=fontSize)
    print("font:",font)

    content = myrandom()  # 获取随机生成的验证码的值
    print("content:",content)
    # 将验证码画到图片上
    draw.text((width * 0.1, height * 0.15), content,
              fill=getColor(), font=font)
    # 画干扰线
    for i in range(5):
        x = random.randint(0, 20)
        y = random.randint(0, height)
        z = random.randint(width - 20, width)
        w = random.randint(0, height)
        draw.line(((x, y), (z, w)), fill=getColor())
    # 返回验证码图片与文本内容
    print("img:",img)
    print("content____:",content)
    return img, content


def getColor():
    '''随机生成一个元组类型的 RGB颜色'''
    color = (random.randint(0, 256), random.randint(0, 256), random.randint(0, 256))
    print("color:",color)
    return color


def myrandom(count=5):
    myList = list(string.ascii_letters + string.digits)  # 指定要生成验证码的集合，数字，大小写字母
    print("mylist first",myList[0])
    # 在指定的mylist集合中随机取出count个集合
    lists = random.sample(myList, count)
    print("mylist",lists)
    # 用指定的字符串连接集合中的内容
    return "".join(lists)


def get_Font(split='.ttf'):
    """返回操作系统下的指定后缀的字体"""
    if os.name == 'posix':
         path = '/Library/Fonts/'
    elif os.name == 'nt':
         path = 'C:\Windows\Fonts'.replace("\\", '/') + '/'
    else:
         # 根据不同的操作系统，系统字体在不同的文件下，Ubuntu下可以在/usr/share/fonts/ 下找到很多文件类型的字体
         path = '/usr/share/fonts/truetype/freefont/'
    #############################不同操作系统下字体文件路径需要改###################################
    #path = 'C:/Windows/Fonts'
    listFont = os.listdir(path)
    # 获取指定后缀的字体，默认是.ttf类型的后缀
    fontList = [path + x for x in listFont if os.path.splitext(x)[1] == split]
    # 返回系统字体列表，以及所在的路径
    # return fontList, path
    # random a font
    path = random.sample(fontList, 1)
    print("path:",path)
    return path[0]

#####################################################################################################
###########check email#####################
from itsdangerous import URLSafeTimedSerializer


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except:
        return False
    return email
@app.route('/confirm/<token>')
def confirm_email(token):
    try:
        email = confirm_token(token)
    except:
        flash('The confirmation link is invalid or has expired.', 'danger')
    #user = User.query.filter_by(email=email).first_or_404()
    result = mongo.db.users.find({'email': email})
    user = list(result)[0]
    if user['confirmed']:
        flash('Account already confirmed. Please login.', 'success')
    else:
        mongo.db.users.update({'email': email},{"$set":{"confirmed":True,"comfirmedtime":datetime.now()}})
        #db.session.add(user)
        #db.session.commit()
        print("okkkkkkkkkkkkk")
        flash('You have confirmed your account. Thanks!', 'success')
    return redirect(url_for('register3'))
'''
@app.route('/resend')
@login_required
def resend_confirmation():
    token = generate_confirmation_token(current_user.email)
    confirm_url = url_for('user.confirm_email', token=token, _external=True)
    html = render_template('user/activate.html', confirm_url=confirm_url)
    subject = "Please confirm your email"
    send_email(current_user.email, subject, html)
    flash('A new confirmation email has been sent.', 'success')
    return redirect(url_for('user.unconfirmed'))
'''