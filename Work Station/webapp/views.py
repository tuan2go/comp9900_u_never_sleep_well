import webapp.admin_view as admin
import webapp.post
import webapp.user


from flask import Flask

from flask import render_template, session, url_for,request,redirect, make_response,send_file, jsonify
from webapp import app
import webapp.searchAPI as sapi



loginPage = 'login'
loginPage2 = 'login.html'
indexPage = "index.html"
searchPage ="search.html"

@app.route('/')
@app.route('/index')
def index():
    if session.get('username'):
        username = session['username']
    else:
        username = None
    num_of_transaction = admin.wp.get_num_of_transactions()
    num_of_posts = admin.wp.get_num_of_posts()
    num_of_users = admin.wp.getNumOfUsers()
    return render_template(indexPage, username=username, num_of_posts=num_of_posts, num_of_users=num_of_users, num_of_transaction=num_of_transaction)



@app.route('/search', methods = ['GET', 'POST'])
def search():
    if session.get('username'):
        username = session['username']
    else:
        username = None
    indate = request.form.get("indate", type=str, default="")
    outdate = request.form.get("outdate", type=str, default="")
    city = request.form.get("city", type=str, default="")
    flag = 1
    return render_template(searchPage,username=username,areaList= sapi.areaList,indate=indate,outdate=outdate,city=city,flag=1)

@app.route('/areaSearch/<city>', methods = ['GET', 'POST'])
def areaSearch(city):
    indate = ""
    outdate = ""
    city = city
    flag = 1
    return render_template(searchPage,areaList= sapi.areaList,indate=indate,outdate=outdate,city=city,flag=1)


@app.route('/submitSearch',methods=['POST'])
def submitSearch():
    indate = request.form.get("indate",type=str,default="")
    outdate = request.form.get("outdate",type=str,default="")
    city = request.form.get("city",type=str,default="")
    flag = request.form.get("flag", type=str, default="")
    if flag != "":
        city = flag
    beds = request.form.get("beds", type=str, default="")
    type = request.form.get("type", type=str, default="")
    bedrooms = request.form.get("bedrooms", type=str, default="")
    bathrooms = request.form.get("bathrooms", type=str, default="")
    price = request.form.get("price", type=str, default="")
    keywords = request.form.get("keywords", type=str, default="")
    checkboxlist = request.form.getlist("check_box_list")
    userid = ""
    if session.get('userid'):
        userid = session['userid']
    results = sapi.search(indate,outdate,city,beds,type,bedrooms,bathrooms,price,keywords,checkboxlist,userid)
    return results




@app.route('/postredirect', methods = ['GET', 'POST'])
def postredirect():

    if session.get('userid'):
        username = session['username']
        return render_template("post.html", username=username)
    else:
        return redirect(loginPage)

@app.route("/image/<userid>/<imageid>")
def images(userid,imageid):
    image = "post-picture/{}/{}".format(userid,imageid)
    print(image)
    return send_file(image,mimetype="image/jpg")


@app.route('/contact_us', methods=['GET', 'POST'])
def contact_us():
    if session.get('username'):
        username = session['username']
    else:
        username = None
    contact_us_feedback = {
        'name': request.form.get('name'),
        'phone': request.form.get('phone'),
        'email': request.form.get('email'),
        'message': request.form.get('message'),
        'state': 'unchecked'
    }
    try:
        if request.form.get('send_message') == 'True':
            print("Success")
            admin.wp.insert_feedback(contact_us_feedback)
    except:
        pass
    return render_template("contact_us.html", username=username)


@app.route('/addLikeRecord',methods=['POST'])
def addListRecord():
    userid = ""
    if session.get('userid'):
        userid = session['userid']
    else:
        return render_template(loginPage2)
    postid = request.form["postid"]
    result = sapi.addLikeRecord(postid,userid)
    return result

@app.route('/getLikeList',methods=['POST'])
def getLikeList():
    userid = ""
    if session.get('userid'):
        userid = session['userid']
        result = sapi.getLikeList(userid)
    else:
        result = sapi.jsonifyData([])
    return result

@app.route('/delLikeRecord',methods=['POST'])
def delListRecord():
    recordid = request.form["recordid"]
    userid = ""
    if session.get('userid'):
        userid = session['userid']
    else:
        return render_template(loginPage2)
    result = sapi.delLikeRecord(recordid,userid)
    return result