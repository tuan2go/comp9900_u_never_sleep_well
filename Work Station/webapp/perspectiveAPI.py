## Google API to detect toxic comments
## input: comment text
## output: score of toxity of comment text, range (0,1)
import webapp.commonFunction as common
from flask_pymongo import ObjectId

mongo = common.initMongo()


def commentCheck(comment):
    from googleapiclient import discovery

    API_KEY='AIzaSyAhPtqcegCk6tAvr1WUWNegNb74osryJJU'

    # Generates API client object dynamically based on service name and version.
    service = discovery.build('commentanalyzer', 'v1alpha1', developerKey=API_KEY)

    analyze_request = {
    'comment': { 'text': comment },
    'requestedAttributes': {'TOXICITY': {}}
    }

    response = service.comments().analyze(body=analyze_request).execute()

    # import json
    # p = json.dumps(response, indent=2)
    # print(p)
    # data = json.load(p)
    return response['attributeScores']['TOXICITY']['summaryScore']['value']
# print(commentCheck('아들둘과 같이 머물었던 앨리네 집은 아이들과'))


# mongo.db.admin_reviews.remove({})
def getReviews():
    review = [
    {
        '$project': {
            'reviews': True
        }
    }
]
    return mongo.db.admin_reviews.aggregate(pipeline=review)

# warn user if his/her comments score more than 0.90, meanwhile, users will receive a warning email
# says "Dear XXX, The system detects that your speech(comment, time) contains vocabulary of attacks and insults.
#  In order to create a harmonious public opinion environment, please speak civilized.
# Continued to make such comments will result in an inability to comment.
# supported by perspectiveAPI from google"

# ban user if his/her comments score more than 0.95 for 3 times, users will be informed to be prohibited by commenting
# scores less than 0.90 is ok

# for m in getReviews():
#     print(m,commentCheck(m['reviews']))
# print(commentCheck("to go what to,to go what toto go what toto go what to stupid"))
def sendWarningEmail(review_detail):
    subject = "Toxic conversation warning"
    message = "<style>h2,h3{font-weight:normal}</style><h2>Dear " + "name_from_database" + "</h2><h3> System detects that your "\
                       "comment("+review_detail['reviews']+") in "+review_detail['time']+" contains vocabulary of "\
                       "attacks and insults.In order to create a harmonious public opinion environment, please speak "\
                       "civilized. Continued to make such comments will be banned from commenting posts.</h3>"\
                       "<h3>Thanks for your cooperation</h3><br><br><br><h3 align='right'>Detecting system applied "\
                       "is supported by <a href='https://www.perspectiveapi.com'>perspectiveAPI</a> from google</h3>"
    common.sendEmail("tuan2goo@gmail.com", subject, 'html', message)


def getLastRecordfromReviews():
    return list(mongo.db.admin_reviews.find().limit(1).sort([('$natural', -1)]))[0]
# print(getLastRecordfromReviews())
# if(commentCheck(getLastRecordfromReviews()['reviews'])>0.90):
#     sendWarningEmail(getLastRecordfromReviews())


def getAllPrice():
    posts = mongo.db.book.find({}, {'property_id': 1, '_id': 0})
    # print(list(posts))
    total_price = 0
    for i in posts:
        price = mongo.db.post.find_one({'_id': ObjectId(i['property_id'])}, {'_id': 0, 'price': 1})
        # print(float(price['price']))
        total_price += float(price['price'])
    return total_price


# print(getAllPrice())
def groupByDate():
    pipe = [
                {
                    '$group': {
                        '_id': {
                            '$toDate': '$time'
                        },
                        'totaldailyIncome': {
                            '$sum': {
                                '$toDouble': '$price'
                            }
                        },
                        'count': {
                            '$sum': 1
                        }
                    }
                }, {
                    '$sort': {
                        '_id': 1
                    }
                }
            ]
    return list(mongo.db.order.aggregate(pipeline=pipe))


def get_all_address_price():
    # pipe = [
    #             {
    #                 '$project': {
    #                     'street': 1,
    #                     'city': 1,
    #                     'price': 1
    #                 }
    #             }
    #         ]
    # return mongo.db.post.aggregate(pipeline=pipe)
    return mongo.db.post.find({},{'street': 1, 'city': 1, 'picture': 1, 'price': 1, '_id': 0}).limit(15)

# print(list(get_all_address_price()))

# mongo.db.users.create_index([("email", "text"), ("username", "text")])
def search_user_by_email_username(target):
    # result = mongo.db.users.find({"$text": {"$search": {'$regex': target}}})
    # result_email = mongo.db.users.find({"email": {"$regex": target, "$options": "$i"}})
    # result_username = mongo.db.users.find({"username": {"$regex": target, "$options": "$i"}})
    result = mongo.db.users.find({
                                    '$or': [
                                                {
                                                    'email': {
                                                        '$regex': target
                                                    }
                                                }, {
                                                    'username': {
                                                        '$regex': target
                                                    }
                                                }
                                            ]
                                },{'email': 1, 'username': 1, 'status': 1, '_id': 0})
    return result, result.count()


def insert_feedback(document):
    mongo.db.feedback.insert_one(document)


def get_all_feedback():
    return mongo.db.feedback.find({}, {'_id': 0})
print(get_all_feedback())


def search_post_by_email(target):
    result = mongo.db.post.find({'userid': {'$regex': target}}, {'userid': 1, 'state': 1, '_id': 0, 'post_time': 1})
    return result, result.count()


# print(list(search_post_by_email()[0]))
def adminAccountCheck(adminname, adminpass):
    result = mongo.db.admin_account.find({'adminname':adminname})
    if result.count() > 0:
        return dict(result='success', message='Admin login successfully!') \
            if list(result)[0]['adminpass'] == str(adminpass) \
            else dict(result='fail', message='Your password is not correct!')
    else:
        return dict(result='error', message='Username does not exist!')


def adminUserDisplay(): return mongo.db.users.find({}, {'_id': 0, 'email': 1, 'username': 1, 'status': 1})


def adminPostDisplay(): return mongo.db.post.find({}, {'userid': 1, 'state': 1, '_id': 0, 'post_time': 1})


def id_find_email(idd): return mongo.db.users.find_one({'_id': ObjectId(idd)}, {'email': 1, '_id': 0})

def getNumOfUsers():    return mongo.db.users.count()


def updateColorOfUser(user, color): mongo.db.users.update({'email':user},{'$set':{'status':color}})


def updateColorOfPost(user, color): mongo.db.admin_post.update({'postid':user},{'$set':{'status':color}})


def deleteUsers(username):  mongo.db.users.delete_one({'email': username})


def delete_feedback(email, name, phone, message):  mongo.db.feedback.delete_one({'email': email, 'name': name, 'phone': phone, 'message': message})

def num_of_feedback(): return mongo.db.feedback.count()

def deletePosts(username):  mongo.db.post.delete_one({'postid':username})


def get_num_of_posts(): return mongo.db.post.count()


def get_num_of_transactions(): return mongo.db.book.count()


# TODO :choose k nearest neighbors for a particular item
# Datasets: static/sample_recommendation_data
# refer to http://www.awesomestats.in/python-recommending-movies/
def k_nearest_neighbours(item_id, k):
    from sklearn.metrics.pairwise import pairwise_distances
    import pandas as pd

    def user_based():
        import numpy as np
        import pandas as pd
        user_movies_df = train.pivot( index='user', columns='item', values = "rate" ).reset_index(drop=True)
        user_movies_df.fillna( 0, inplace = True )
        user_sim = 1 - pairwise_distances( user_movies_df.as_matrix(), metric="cosine" )
        user_sim_df = pd.DataFrame( user_sim )
        np.fill_diagonal( user_sim, 0 )
        user_sim_df = pd.DataFrame( user_sim )
        print(user_sim_df)

    def get_similar_items( item_id, topN = k ):
        item_df['similarity'] = item_sim_df.iloc[item_id -1]
        top_n = item_df.sort_values( ["similarity"], ascending = False )[0:topN]
        print( "Similar", k, "items with item", item_id, ":")
        return top_n

    train = pd.read_csv('static/sample_recommendation_data/recommendation_data', sep=',')
    rating_mat = train.pivot( index='item', columns='user', values = "rate" ).reset_index(drop=True)
    rating_mat.fillna( 0, inplace = True )
    # print(rating_mat)
    item_sim = 1 - pairwise_distances( rating_mat.as_matrix(), metric="correlation" )
    # print(movie_sim)
    item_sim_df = pd.DataFrame( item_sim )
    # print(movie_sim_df)
    item_df = pd.read_csv('static/sample_recommendation_data/item',sep=',')
    item_df.columns = ['movieid', 'title']
    # movies_df['similarity'] = movie_sim_df.iloc[0]
    # movies_df.columns = ['movieid', 'title', 'similarity']
    # print(movies_df)
    # print()
    print(get_similar_items(item_id))

# k_nearest_neighbours(1, 5)


# content based recommendation system
# based on 'type'(35%) 'city'(35%) 'beds'(10%) 'bedrooms'(10%) 'bathrooms'(10%)
def content_based_recommendation_system(post_id, n):
    num_of_posts = get_num_of_posts()
    n = min(n, num_of_posts)
    result = mongo.db.post.find_one({'_id': ObjectId(post_id)}, {'type': 1, 'city': 1, 'beds': 1, 'bedrooms': 1, 'bathrooms': 1})
    room_type = result['type']
    city = result['city']
    beds = result['beds']
    bedrooms = result['bedrooms']
    bathrooms = result['bathrooms']
    matrix = [[0 for _ in range(5)] for _ in range(num_of_posts)]
    all_posts = mongo.db.post.find({}, {'type': 1, 'city': 1, 'beds': 1, 'bedrooms': 1, 'bathrooms': 1})
    index = 0
    map = {}
    for post in all_posts:
        # matrix[index][0] = 1 if post['_id'] == post_id else 0
        matrix[index][0] = 0.35 if post['type'] == room_type else 0
        matrix[index][1] = 0.35 if post['city'] == city else 0
        matrix[index][2] = 0.1 if post['beds'] == beds else 0
        matrix[index][3] = 0.1 if post['bedrooms'] == bedrooms else 0
        matrix[index][4] = 0.1 if post['bathrooms'] == bathrooms else 0
        map[post['_id']] = matrix[index]
        index += 1
    # print(matrix)
    return sorted(map, key=lambda m: sum(map[m]), reverse=True)[1:n+1]
    # for i in apapap:
    #     print(i ,map[i])
    # print(sort_matrix)


def find_limit(email):
    return mongo.db.users.find_one({'email':email, 'status': 'l'}, {'_id':1, 'email': 1, 'username': 1})


print(find_limit('tuan2goo@gmail.com'))