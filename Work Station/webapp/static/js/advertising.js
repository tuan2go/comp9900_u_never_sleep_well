var imgFile = []; //文件流
var imgSrc = []; //图片路径
var imgName = []; //图片名字
var latitude=0;
var longitude=0;
$(function(){
	// 鼠标经过显示删除按钮
	$('.content-img-list').on('mouseover','.content-img-list-item',function(){
		$(this).children('a').removeClass('hide');
	});
	// 鼠标离开隐藏删除按钮
	$('.content-img-list').on('mouseleave','.content-img-list-item',function(){
		$(this).children('a').addClass('hide');
	});
	// 单个图片删除
	$(".content-img-list").on("click",'.content-img-list-item a',function(){
	    	var index = $(this).attr("index");
			imgSrc.splice(index, 1);
			imgFile.splice(index, 1);
			imgName.splice(index, 1);
			var boxId = ".content-img-list";
			addNewContent(boxId);
			if(imgSrc.length<7){//显示上传按钮
				$('.content-img .file').show();
			}
	  });
	//图片上传
	$('#upload').on('change',function(){

		if(imgSrc.length>=7){
			return alert("最多只能上传4张图片");
		}
		var imgSize = this.files[0].size;  //b
		if(imgSize>1024*1024*1){//1M
			return alert("上传图片不能超过1M");
		}
		console.log(this.files[0].type)
		if(this.files[0].type != 'image/png' && this.files[0].type != 'image/jpeg' && this.files[0].type != 'image/gif'){
			return alert("图片上传格式不正确");
		}

		var imgBox = '.content-img-list';
		var fileList = this.files;
		for(var i = 0; i < fileList.length; i++) {
			var imgSrcI = getObjectURL(fileList[i]);
			imgName.push(fileList[i].name);
			imgSrc.push(imgSrcI);
			imgFile.push(fileList[i]);
		}
		if(imgSrc.length==7){//隐藏上传按钮
			$('.content-img .file').hide();
		}
		addNewContent(imgBox);
		/*this.value = null;//解决无法上传相同图片的问题*/
	});

	//提交请求
    $('#btn-submit-upload').on('click',function(){
        var formFile = new FormData();
        var i =0;

        //check form value
        //colum1
        if ($("#city").val()==""){
            $("#city").css("border-color","red");
            i++;
        }
        else{
            $("#city").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("city",$("#city").val());
        }
        if ($("#Street-Address").val()==""){
            $("#Street-Address").css("border-color","red");
            i++;
        }
        else{
            $("#Street-Address").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("Street-Address",$("#Street-Address").val());
        }
        if ($("#Postal-code").val()==""){
            $("#Postal-code").css("border-color","red");
            i++;
        }
        else{
            $("#Postal-code").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("Postal-code",$("#Postal-code").val());
        }
        if (latitude==0){
            $("#Postal-code").css("border-color","red");
            $("#city").css("border-color","red");
            $("#Street-Address").css("border-color","red");
            i++;
        }
        else{
            $("#Street-Address").css("border-color","rgba(141, 141, 141, 0.91)");
            $("#city").css("border-color","rgba(141, 141, 141, 0.91)");
            $("#Postal-code").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("latitude",latitude);
        }
        if (longitude==0){
            $("#Postal-code").css("border-color","red");
            $("#city").css("border-color","red");
            $("#Street-Address").css("border-color","red");
            i++;
        }
        else{
            $("#Street-Address").css("border-color","rgba(141, 141, 141, 0.91)");
            $("#city").css("border-color","rgba(141, 141, 141, 0.91)");
            $("#Postal-code").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("longitude",longitude);
        }


        if ($("#city").val()==""||$("#Street-Address").val()==""||$("#Postal-code").val()==""||latitude==0||longitude==0){
             $("#colum1").children("span").css("display","");
        }else{
            $("#colum1").children("span").css("display","none");
        }


        //colum2
        if ($("#type").val()==""){
            $("#type").parent(".form-select").children(".minict_wrapper").css("border-color","red");
            i++;
        }
        else{
            $("#type").parent(".form-select").children(".minict_wrapper").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("type",$("#type").val());
        }
        if ($("#bedrooms").val()==""){
            $("#bedrooms").parent(".form-select").children(".minict_wrapper").css("border-color","red");
            i++;
        }
        else{
            $("#bedrooms").parent(".form-select").children(".minict_wrapper").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("bedrooms",$("#bedrooms").val());
        }
        if ($("#bathrooms").val()==""){
            $("#bathrooms").parent(".form-select").children(".minict_wrapper").css("border-color","red");
            i++;
        }
        else{
            $("#bathrooms").parent(".form-select").children(".minict_wrapper").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("bathrooms",$("#bathrooms").val());
        }
        if ($("#beds").val()==""){
            $("#beds").parent(".form-select").children(".minict_wrapper").css("border-color","red");
            i++;
        }
        else{
            $("#beds").parent(".form-select").children(".minict_wrapper").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("beds",$("#beds").val());
        }
        if ($("#price").val()==""){
            $("#price").css("border-color","red");
            i++;
        }
        else{
            $("#price").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("price",$("#price").val());
        }
        if ($("#start_time").val()==""){
            $("#start_time").css("border-color","red");
            i++;
        }
        else{
            $("#start_time").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("start_time",$("#start_time").val());
        }
        if ($("#end_time").val()==""){
            $("#end_time").css("border-color","red");
            i++;
        }
        else{
            $("#end_time").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("end_time",$("#end_time").val());
        }
        if ($("#name").val()==""){
            $("#name").css("border-color","red");
            i++;
        }
        else{
            $("#name").css("border-color","rgba(141, 141, 141, 0.91)");
            formFile.append("name",$("#name").val());
        }


        if ($("#type").val()==""||$("#bedrooms").val()==""||$("#bathrooms").val()==""||$("#beds").val()==""||$("#price").val()==""||$("#end_time").val()==""||$("#start_time").val()==""||$("#name").val()==""){
             $("#colum2").children("span").css("display","");
        }else{
            $("#colum2").children("span").css("display","none");
        }


        //colum3
        if(imgFile==false){
            $("#colum3").children("span").css("display","");
            i++;
//            alert("photoooo");
        }else{
            $("#colum3").children("span").css("display","none");
            $.each(imgFile, function(i, file){
            formFile.append('myFile[]', file);
            });
        }
//        alert(i);
        //colum4
        if($("#checkbox-1").prop('checked') == true) {
            formFile.append('wifi', true);
        }
        if($("#checkbox-2").prop('checked') == true) {
            formFile.append('parking', true);
        }
        if($("#checkbox-3").prop('checked') == true) {
            formFile.append('gym', true);
        }
        if($("#checkbox-4").prop('checked') == true) {
            formFile.append('pets', true);
        }
        if($("#checkbox-5").prop('checked') == true) {
            formFile.append('air_condition', true);
        }
        if($("#checkbox-6").prop('checked') == true) {
            formFile.append('kitchen', true);
        }
        if($("#checkbox-7").prop('checked') == true) {
            formFile.append('linens', true);
        }
        if($("#checkbox-8").prop('checked') == true) {
            formFile.append('washing_machine', true);
        }
        if($("#checkbox-9").prop('checked') == true) {
            formFile.append('tv', true);
        }
        if($("#checkbox-10").prop('checked') == true) {
            formFile.append('pool', true);
        }
        if($("#checkbox-11").prop('checked') == true) {
            formFile.append('lift', true);
        }
        if($("#checkbox-12").prop('checked') == true) {
            formFile.append('facilities_for_disabled', true);
        }
        if ($("#describe").val()!=""){
            formFile.append("describe",$("#describe").val());
        }


//        for(var x of formFile){
//        alert(x);//这样我们就能在控制台看到我们发送的全部数据了
//        }

        // FormData上传图片
        // formFile.append("type", type);
        // formFile.append("content", content);
        // formFile.append("mobile", mobile);
        // 遍历图片imgFile添加到formFile里面


        //console.log(imgFile)
         if (i==0){
            $.ajax({
             url: '/post',
             type: 'POST',
             data: formFile,
             async: true,
             cache: false,
             contentType: false,
             processData: false,
             // traditional:true,
             dataType:'json',
             success: function(res) {
                 //alert(res);
                 if(res["code"]==0){
//                     alert("success");
                     $("#searchNotification").html("List successfully")
$('#searchNotification').show();
setTimeout(function(){
    $('#searchNotification').hide(300);
},5000);
                     window.location.href="/post_detail/"+res["id"];
         //             $("#adviceContent").val("");
    	 			// $("#contact").val("");
                 }else{
                 $("#searchNotification").html("List failed")
$('#searchNotification').show();
setTimeout(function(){
    $('#searchNotification').hide(300);
},5000);
                 }
             }
         })
         }
    });

    //review
    $('#review_submit').unbind('click').on('click',function(){
        //alert("review");
        if ($("#review_content").val()==""){
            $("#review_content").css("border-color","red");
            $("#property_reply").children("span").css("display","");
        }
        else{
            var formFile = new FormData();
            formFile.append("review_content",$("#review_content").val());
            formFile.append("property_id",$("#property_id").val());
            $.ajax({
             url: '/reviews',
             type: 'POST',
             data: formFile,
             async: true,
             cache: false,
             contentType: false,
             processData: false,
             // traditional:true,
             dataType:'json',
             success: function(res) {
                 if(res["code"]==0){
                     //alert("success");
                     $("#searchNotification").html("Success reply")
$('#searchNotification').show();
setTimeout(function(){
    $('#searchNotification').hide(300);
},5000);
                     window.location.href="/post_detail/"+res['id'];
         //             $("#adviceContent").val("");
    	 			// $("#contact").val("");
                 }else{
                     //alert("Your review contains sensitive words");
                      $("#searchNotification").html("Sensitive words contained")
$('#searchNotification').show();
setTimeout(function(){
    $('#searchNotification').hide(300);
},5000);
                 }
             }
         })
        }
	});


    //map
    $("#Postal-code").on("change",function(){
        if ($("#city").val()=="" || $("#Street-Address").val()==""){

        }else{
            initialize(13);
            codeAddress();
        }
	 });
	$("#city").on("change",function(){
        if ($("#Postal-code").val()=="" || $("#Street-Address").val()==""){

        }else{
            initialize(13);
            codeAddress();
        }
	 });
	$("#Street-Address").on("change",function(){
        if ($("#city").val()=="" || $("#Postal-code").val()==""){

        }else{
            initialize(13);
            codeAddress();
        }
	 });
    //show
    $('#book_submit').unbind('click').on('click',function(){
        //alert("book");
        if ($("#chstart_time").val()==""){
            $("#chstart_time").css("border-color","red");
        }
        else if ($("#chend_time").val()==""){
            $("#chend_time").css("border-color","red");
        }
        else{
            var formFile = new FormData();
            formFile.append("chend_time",$("#chend_time").val());
            formFile.append("chstart_time",$("#chstart_time").val());
            formFile.append("property_id",$("#pi").val());
            $.ajax({
             url: '/book',
             type: 'POST',
             data: formFile,
             async: true,
             cache: false,
             contentType: false,
             processData: false,
             // traditional:true,
             dataType:'json',
             success: function(res) {
                 if(res["code"]==0){
                     //alert("success");
                     $("#searchNotification").html("Book successfully")
$('#searchNotification').show();
setTimeout(function(){
    $('#searchNotification').hide(300);
},5000);
                     window.location.href="/post_detail/"+res['id'];
         //             $("#adviceContent").val("");
    	 			// $("#contact").val("");
                 }else{
                     //alert("fail");
                     $("#searchNotification").html("Book failed")
$('#searchNotification').show();
setTimeout(function(){
    $('#searchNotification').hide(300);
},5000);
                 }
             }
         })
        }
	});
});

//删除
function removeImg(obj, index) {
	imgSrc.splice(index, 1);
	imgFile.splice(index, 1);
	imgName.splice(index, 1);
	var boxId = ".content-img-list";
	addNewContent(boxId);
}

//图片展示
function addNewContent(obj) {
	// console.log(imgSrc)
	$(obj).html("");
	for(var a = 0; a < imgSrc.length; a++) {
		var oldBox = $(obj).html();
		$(obj).html(oldBox + '<li class="content-img-list-item"><img src="'+imgSrc[a]+'" alt=""><a index="'+a+'" class="hide delete-btn"><i class="ico-delete"></i></a></li>');
	}
}

//建立一個可存取到該file的url
function getObjectURL(file) {
	var url = null ;
	if (window.createObjectURL!=undefined) { // basic
		url = window.createObjectURL(file) ;
	} else if (window.URL!=undefined) { // mozilla(firefox)
		url = window.URL.createObjectURL(file) ;
	} else if (window.webkitURL!=undefined) { // webkit or chrome
		url = window.webkitURL.createObjectURL(file) ;
	}
	return url ;
}


//map
var geocoder;
var map;
function initialize(j) {
    //alert("jinlaimei");
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-32.866, 147.196);
    var myOptions = {
      zoom: j,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
}

function codeAddress() {
    var code = document.getElementById("Postal-code").value;
    var street = document.getElementById("Street-Address").value;
    var city = document.getElementById("city").value;
    var address = street+', '+city+', '+code;
    latitude=0;
    longitude=0;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        //alert(results[0].geometry.location)
        latitude = results[0].geometry.location.lat();
        longitude = results[0].geometry.location.lng();
        map.setCenter(results[0].geometry.location);
        this.marker = new google.maps.Marker({
                title:address,
            map: map,
            position: results[0].geometry.location
        });
                var infowindow = new google.maps.InfoWindow({
                    content: '<strong>'+address+'</strong><br/>'+'Latitude: '+results[0].geometry.location.lat()+'<br/>Longitude: '+results[0].geometry.location.lng()
                });
                infowindow.open(map,marker);

      } else {
        //alert("Unavailable address: Please check your input");
        $("#searchNotification").html("Unavailable address")
$('#searchNotification').show();
setTimeout(function(){
    $('#searchNotification').hide(300);
},3000);
      }
    });
}


function initiamap(j){
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-32.866, 147.196);
    var myOptions = {
      zoom: j,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    var lati = document.getElementById("lat").value;
    var lon = document.getElementById("lon").value;
    var address = document.getElementById("add").value;
    //alert(lati);
    //alert(lon);
    var pos = new google.maps.LatLng(lati, lon);
    //alert(pos);
    map.setCenter(pos);
    this.marker = new google.maps.Marker({
                title:address,
            map: map,
            position: pos
        });
                    var infowindow = new google.maps.InfoWindow({
                    content: '<strong>'+address+'</strong><br/>'+'Latitude: '+lati+'<br/>Longitude: '+lon
                });
                infowindow.open(map,marker);
}