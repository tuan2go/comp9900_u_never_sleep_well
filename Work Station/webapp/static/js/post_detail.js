    $(document).on('ready', function() {
      $(".variable").slick({
        dots: true,
        infinite: true,
        variableWidth: true
      });

      var diadate= eval($("#booked").val());
      //alert(diadate);
      $('#chstart_time').pickadate({
        format: 'yyyy/mm/dd',
        formatSubmit: 'yyyy/mm/dd',
        min: new Date(),
        disable:diadate,
      });

       $('#chend_time').pickadate({
        format: 'yyyy/mm/dd',
        formatSubmit: 'yyyy/mm/dd',
        min: new Date(),
        disable: diadate,
      });
    });