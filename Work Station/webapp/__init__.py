"""
The flask application package.
"""
from flask import Flask
from flask_pymongo import PyMongo
import os


app = Flask(__name__)
app.config['SECRET_KEY'] = '\xca\x0c\x86\x04\x98@\x02b\x1b7\x8c\x88]\x1b\xd7"+\xe6px@\xc3#\\'
app.config['basedir'] = os.path.abspath(os.path.dirname(__file__))
app.config['SECURITY_PASSWORD_SALT'] = 'webweb_app'

import webapp.views

